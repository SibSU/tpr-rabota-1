﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab1WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Время работ А и В
        private int tA, tB;
        // Массив времени работ C, D и E
        private int[] tP;
        // Резерв
        private int R;

        // Время завершения работ
        Dictionary<int, int> tablePeriodMoney;

        // Таблица решений
        Dictionary<int, int> _variable;
        // Таблица вероятностей
        Dictionary<int, double> _probability;

        public MainWindow()
        {
            InitializeComponent();

            btnSolve.Click += BtnSolve_Click;
            btnClear.Click += BtnClear_Click;
        }

        private void BtnSolve_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Initial();
                FillVariable();
                FillProbability();
                Logging();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Метод <c>Initial</c> обрабатывает данные задачи
        /// </summary>
        private void Initial()
        {
            try
            {
                // Считываем данные
                Int32.TryParse(txtA.Text, out tA);
                Int32.TryParse(txtB.Text, out tB);
                tP = new int[3];
                Int32.TryParse(txtC.Text, out tP[0]);
                Int32.TryParse(txtD.Text, out tP[1]);
                Int32.TryParse(txtE.Text, out tP[2]);

                Int32.TryParse(txtR.Text, out R);


                // Считываем время и прибыль
                int key, value;
                tablePeriodMoney = new Dictionary<int, int>();
                Int32.TryParse(txtPeroid0.Text, out key);
                Int32.TryParse(txtMoney0.Text, out value);
                tablePeriodMoney.Add(key, value);
                Int32.TryParse(txtPeroid1.Text, out key);
                Int32.TryParse(txtMoney1.Text, out value);
                tablePeriodMoney.Add(key, value);
                Int32.TryParse(txtPeroid2.Text, out key);
                Int32.TryParse(txtMoney2.Text, out value);
                tablePeriodMoney.Add(key, value);
                Int32.TryParse(txtPeroid3.Text, out key);
                Int32.TryParse(txtMoney3.Text, out value);
                tablePeriodMoney.Add(key, value);
                Int32.TryParse(txtPeroid4.Text, out key);
                Int32.TryParse(txtMoney4.Text, out value);
                tablePeriodMoney.Add(key, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Метод <c>FillVariable</c> заполняет словарь <c>variable</c> возможными исходами
        /// </summary>
        private void FillVariable()
        {
            _variable = new Dictionary<int, int>();

            for (int i = 0; i < 3; i++) //C
            {
                for (int j = 0; j < 3; j++) //D   
                {
                    for (int k = 0; k < 3; k++) //E
                    {
                        int temp = Math.Max(tP[i], Math.Max(tA + tP[j], tB + tP[k]));
                        int value = 0;
                        if (!_variable.TryGetValue(temp, out value))
                        {
                            value++;
                            _variable.Add(temp, value);
                        }
                        else
                        {
                            _variable.Remove(temp);
                            value++;
                            _variable.Add(temp, value);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Метод <c>FillProbability</c> заполняет словарь вероятностей
        /// </summary>
        private void FillProbability()
        {
            _probability = new Dictionary<int, double>();

            int allVarable = _variable.Values.ToArray().Sum();

            foreach (int key in _variable.Keys)
            {
                int temp;
                _variable.TryGetValue(key, out temp);
                _probability.Add(key, (double)temp / allVarable);
            }
        }

        private void Logging()
        {
            txtLog.Text = "";

            LogProbability(_probability, txtLog);

            LogTable(tablePeriodMoney, _probability, R, txtLog);
            
            txtLog.Text += "ВЫВОД\n\n";

            // Принцип максимального правдоподобия
            LogMaxProbability(tablePeriodMoney, _probability, R, txtLog);

            // Принцип максимального правдоподобия
            LogBayes(tablePeriodMoney, _probability, R, txtLog);

            // Принцип максимального правдоподобия
            LogGuarantee(tablePeriodMoney, _probability, R, txtLog);
        }

        private void LogProbability(Dictionary<int, double> probability, TextBox logging)
        {
            var t = probability.Keys.ToArray();
            Array.Sort(t);

            // Вывод вероятностей
            logging.Text += "Вывод вероятностей\n";
            foreach (int key in t)
            {
                double temp;
                probability.TryGetValue(key, out temp);
                logging.Text += "T=" + key + "\tP=" + temp + "\n";
            }
            logging.Text += "\n\n";
        }

        private void LogTable(Dictionary<int, int> periodToMoney, Dictionary<int, double> probability, int reserve, TextBox logging)
        {
            var t = probability.Keys.ToArray();
            Array.Sort(t);

            // Шапка таблицы
            logging.Text += "Таблица использвания и не использования резерва\n";
            logging.Text += "\t";
            foreach (int key in t)
            {
                logging.Text += "T=" + key + "\t";
            }
            logging.Text += "\n";

            // Не используя резерв
            logging.Text += "x=0\t";
            foreach (int key in t)
            {
                int temp;
                periodToMoney.TryGetValue(key, out temp);
                logging.Text += temp + "\t";
            }
            logging.Text += "\n";

            // Используя резерв
            logging.Text += "x=1\t";
            foreach (int key in t)
            {
                int temp;
                periodToMoney.TryGetValue(key - 1, out temp);
                logging.Text += (temp - R) + "\t";
            }
            logging.Text += "\n\n\n";
        }

        /// <summary>
        /// Метод <c>LogBayes</c> выводит в <c>TextBox</c> решение принципом максимального правдоподобия
        /// </summary>
        /// <param name="periodToMoney">Словарь выручки key - срок работ, value - выручка</param>
        /// <param name="probability">Словарь вариантов решений key - общий срок работ, value - вероятность</param>
        /// <param name="reserve">Резерв</param>
        /// <param name="logging"><c>TextBox</c> в который доавлять решение</param>
        private void LogMaxProbability(Dictionary<int, int> periodToMoney, Dictionary<int, double> probability, int reserve, TextBox logging)
        {
            txtLog.Text += "1. Принцип максимального правдоподобия\n";
            
            // Опеределяем максимальное правдоподобие
            int max;
            max = probability.Keys.ToArray().First();
            foreach (int value in probability.Keys)
            {
                double temp;
                double maxProbability;
                probability.TryGetValue(value, out temp);
                probability.TryGetValue(max, out maxProbability);
                if (temp > maxProbability)
                {
                    max = value;
                }
            }

            // Считаем выручку с резервом и без
            int x0, x1;
            periodToMoney.TryGetValue(max, out x0);
            periodToMoney.TryGetValue(max - 1, out x1);
            x1 -= reserve;

            logging.Text += "Прибыль не используя резерв = " + x0 + " (срок " + max + " лет)\n";
            logging.Text += "Прибыль используя резерв = " + x1 + " (срок " + (max - 1) + " лет)\n";
            logging.Text += "Следовательно, использовать резерв " + (x0 < x1 ? "целесообразно" : x0 > x1 ? "нецелесообразно" : "возможно") + "\n";
            txtLog.Text += "\n\n";
        }

        /// <summary>
        /// Метод <c>LogBayes</c> выводит в <c>TextBox</c> решение принципом Байеса
        /// </summary>
        /// <param name="pMoney">Словарь выручки key - срок работ, value - выручка</param>
        /// <param name="probability">Словарь вариантов решений key - общий срок работ, value - вероятность</param>
        /// <param name="reserve">Резерв</param>
        /// <param name="logging"><c>TextBox</c> в который доавлять решение</param>
        private void LogBayes(Dictionary<int, int> pMoney, Dictionary<int, double> probability, int reserve, TextBox logging)
        {
            logging.Text += "2. Принцип Байеса\n";

            var t = probability.Keys.ToArray();
            Array.Sort(t);

            // Считаем мат.ожидание с выручкой и без
            double x0 = 0, x1 = 0;
            foreach (int key in t)
            {
                int money;
                double temp;
                pMoney.TryGetValue(key, out money);
                probability.TryGetValue(key, out temp);
                x0 += money * temp;
            }
            foreach (int key in t)
            {
                int money;
                double temp;
                pMoney.TryGetValue(key - 1, out money);
                probability.TryGetValue(key, out temp);
                x1 += (money - reserve) * temp;
            }

            logging.Text += "Прибыль не используя резерв = " + x0 + "\n";
            logging.Text += "Прибыль используя резерв = " + x1 + "\n";
            logging.Text += "Следовательно, использовать резерв " + (x0 < x1 ? "целесообразно" : x0 > x1 ? "нецелесообразно" : "возможно") + "\n";
            logging.Text += "\n\n";

        }

        /// <summary>
        /// Метод <c>LogBayes</c> выводит в <c>TextBox</c> решение принципом гарантированных оценок
        /// </summary>
        /// <param name="pMoney">Словарь выручки key - срок работ, value - выручка</param>
        /// <param name="probability">Словарь вариантов решений key - общий срок работ, value - вероятность</param>
        /// <param name="reserve">Резерв</param>
        /// <param name="logging"><c>TextBox</c> в который доавлять решение</param>
        private void LogGuarantee(Dictionary<int, int> pMoney, Dictionary<int, double> probability, int reserve, TextBox logging)
        {
            txtLog.Text += "3. Принцип гарантированных оценок\n";

            // Определяем наихудший вариант, т.е. самый долгий срок работ
            int max =probability.Keys.ToArray().Max();

            // Считаем выручку с резервом и без
            int x0 = 0, x1 = 0;
            pMoney.TryGetValue(max, out x0);
            pMoney.TryGetValue(max - 1, out x1);
            x1 -= reserve;
            
            logging.Text += "Прибыль не используя резерв = " + x0 + " (срок " + max + " лет)\n";
            logging.Text += "Прибыль используя резерв = " + x1 + " (срок " + (max - 1) + " лет)\n";
            logging.Text += "Следовательно, использовать резерв " + (x0 < x1 ? "целесообразно" : x0 > x1 ? "нецелесообразно" : "возможно") + "\n";
            txtLog.Text += "\n\n";
        }
    }
}
